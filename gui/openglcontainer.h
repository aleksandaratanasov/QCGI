#ifndef OPENGLCONTAINER_HPP
#define OPENGLCONTAINER_HPP

#include <QWidget>
#include <QGLFunctions>
#include <QGLWidget>
#include <QPoint>

// The control for the OpenGL widget was insipred by http://www.bogotobogo.com/Qt/Qt5_OpenGL_QGLWidget.php
class OpenGLContainer : public QGLWidget, protected QGLFunctions
{
    Q_OBJECT
public:
  explicit OpenGLContainer(QWidget *parent = 0);

signals:  // Following signals can be used to externally detect changes that happen inside the widget
  // Rotate around x, y and z axes using mouse
  void xRotationChanged(double angle);
  void yRotationChanged(double angle);
  void zRotationChanged(double angle);
  // Translate along x, y and z (zoom) axes using the mouse
  void xTranslationChanged(double x);
  void yTranslationChanged(double y);
  void zTranslationChanged(double z);

public slots:
  /**
   * @brief Changes the polygon mode that is used
   * @param mode Can be 0 for solid or 1 for wireframe
   */
  void changeViewMode(int mode);
  /**
   * @brief Resets the translation along the Z axis to its initial value of -2.0
   */
  void zoomReset();
  /**
   * @brief Translates the camera long the Z axis toward the scene (same as scrolling up
   *        with the mouse wheel)
   */
  void zoomIn();
  /**
   * @brief Translates the camera long the Z axis outward the scene (same as scrolling
   *        down with the mouse wheel)
   */
  void zoomOut();
  /**
   * @brief Resets the translation along the X and Y axes to their initial values of 1.0
   */
  void resetTranslation();
  /**
   * @brief Resets the rotation around the X, Y and Z axes to their initial values of 1.0
   */
  void resetRotation();

private:
  int viewMode;
  double xRot;
  double yRot;
  double zRot;
  QPoint lastPos;
  double xTrans;
  double yTrans;
  double zZoom;  // zTrans
  bool toggleTranslation;

  private:
    /**
     * @brief Initializes the OpenGL environment
     */
    void initializeGL();
    /**
     * @brief Triggered when redrawing is required (see @ref updageGL() or @ref update())
     *        Example: resizing, hiding etc. the widget require redrawing
     */
    void paintGL();
    /**
     * @brief Triggered when resizing the widget (also triggers @ref paintGL())
     * @param w New width of the widget
     * @param h New height of the widget
     */
    void resizeGL(int w, int h);
    /**
     * @brief Handles the pressing of mouse buttons incl. the mouse wheel
     * @param event Gives information about which mouse button has been pressed
     */
    void mousePressEvent(QMouseEvent *event);
    /**
     * @brief Handles the movement of the mouse (coordinates of the cursor)
     * @param event Hives information about the mouse movement (used for the translation
     *              along the X and Y axes and rotation around all three axes)
     */
    void mouseMoveEvent(QMouseEvent *event);
    /**
     * @brief Handles the scrolling of the mouse wheel
     * @param event Gives information about the rotation of the mouse wheel (used for the
     *              translation along the X and Y axes and rotation around all three axes)
     */
    void wheelEvent(QWheelEvent *event);
    // Rotation
    /**
     * @brief Sets the angle around the X axis
     * @param angle Changed value for the rotation angle around the X axis (used for the
     *        translation along the Z axis)
     */
    void setXRotation(double angle);
    /**
     * @brief Sets the angle around the Y axis
     * @param angle Changed value for the rotation angle around the Y axis
     */
    void setYRotation(double angle);
    /**
     * @brief Sets the angle around the Z axis
     * @param angle Changed value for the rotation angle around the Z axis
     */
    void setZRotation(double angle);
    // Translation
    /**
     * @brief Translates the camera along the X and Y axes using mouse input (left button + drag)
     * @param speedX The speed of the mouse cursor along the X axis
     * @param speedY The speed of the mouse cursor along the Y axis
     */
    void setXYTranslation(int speedX, int speedY);     // Up/Down and Left/Right
    /**
     * @brief Translates the camera along the Z axis using mouse input (wheel)
     * @param zoomDirection
     */
    void setZoom(int zoomDirection);      // Nearer/Closer to viewer
};

#endif // OPENGLCONTAINER_HPP
