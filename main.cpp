#include "QApplication"
#include "gui/openglcontainer.h"

int main(int argc, char *argv[]){
    QApplication app(argc, argv);
    OpenGLContainer oglc;
    oglc.resize(640,480);
    oglc.show();
    return app.exec();
}
